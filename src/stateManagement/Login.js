import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {ToastAndroid} from 'react-native';

const Login = ({navigation, route}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const url = 'https://staging.api.autotrust.id/api/v1/';

  const dispatch = useDispatch();
  const {isLoggedIn} = useSelector(state => state.auth);

  const onLogin = async () => {
    const data = {
      email,
      password,
    };
    try {
      const response = await fetch(`${url}user/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      const result = await response.json();
      console.log('Success:', result);
      if (result.code === 200) {
        const data = result;
        dispatch({type: 'LOGIN_SUCCESS', data});
        ToastAndroid.showWithGravity(
          'Login Sukses',
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
        navigation.replace('Home');
      }
    } catch (error) {
      console.error('Error:', error);
      ToastAndroid.showWithGravity(
        'Login gagal, email atau password salah',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );
    }
  };

  React.useEffect(() => {
    if (isLoggedIn) {
      navigation.replace('Home');
    }
  }, []);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 20, color: '#000', fontWeight: '700'}}>
        {' '}
        Login 5.2{' '}
      </Text>
      <View style={{width: '80%', marginTop: 15}}>
        <Text>Masukkan Email</Text>
        <TextInput
          onChangeText={Text => setEmail(Text)}
          placeholder="Masukkan Email"
          keyboardType="email-address"
          style={{
            width: '100%',
            borderRadius: 6,
            borderColor: '#dedede',
            borderWidth: 1,
            paddingHorizontal: 10,
            marginTop: 15,
          }}
        />
      </View>
      <View style={{width: '80%', marginTop: 15}}>
        <Text>Masukkan Password</Text>
        <TextInput
          onChangeText={Text => setPassword(Text)}
          placeholder="Masukkan Password"
          secureTextEntry={true}
          style={{
            width: '100%',
            borderRadius: 6,
            borderColor: '#dedede',
            borderWidth: 1,
            paddingHorizontal: 10,
            marginTop: 15,
          }}
        />
      </View>
      <TouchableOpacity
        onPress={() => onLogin()}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 10,
          paddingHorizontal: 12,
          backgroundColor: 'green',
          borderRadius: 6,
          marginTop: 20,
        }}>
        <Text style={{color: '#fff', fontSize: 14, fontWeight: '500'}}>
          Log in
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;
